

public class Main {

    public static void main(String[] args) {

        int[] myArr = {1,1,1,1,1,1,1,1,1,1};
        int[] myArr2 = {2,6,7,2,5,7,8,3,1,5};


        System.out.println(sum(myArr));
        System.out.println(product(myArr));

        System.out.println(sum(myArr2));
        System.out.println(product(myArr2));
    }

    private static int sum(int[] arr){
        int sum = 0;
        for(int i = 0;i<arr.length;i++)
            sum = sum + arr[i];
        return sum;
    }

    private static long product(int[] arr){
        long product = 1;
        for(int i=0;i<arr.length;i++)
            product = product * arr[i];
        return product;
    }

}
